import 'package:ejemplo/src/detalle_partida.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const BlocApp());

}
class BlocApp extends StatelessWidget {
  const BlocApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App',
      home: DetallePartida(),
      debugShowCheckedModeBanner: false,
    );
  }
}
