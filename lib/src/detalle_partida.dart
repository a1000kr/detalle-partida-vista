
import 'package:flutter/material.dart';
import 'package:partida/partida.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_tabler_icons/flutter_tabler_icons.dart';
class DetallePartida extends StatefulWidget {
  const DetallePartida({Key? key }) : super(key: key);

  @override
  _DetallePartidaState createState() => _DetallePartidaState();
}

class _DetallePartidaState extends State<DetallePartida> {
  List<charts.Series<Partida2, String>> _infromacionPartida = [];
  int i =0;
  
  _llenarLista(){
    var ronda1 = [
      Partida2(jugador: "Pedro", cartasAzules: 1, cartasVerdes: 0, cartasRosas: 0, cartasNegras: 0),
      Partida2(jugador: "Alfonso", cartasAzules: 0, cartasVerdes: 0, cartasRosas: 0, cartasNegras: 0),
    ];
    var ronda2 = [
      Partida2(jugador: "Pedro", cartasAzules: 1, cartasVerdes: 1, cartasRosas: 0, cartasNegras: 0),
      Partida2(jugador: "Alfonso", cartasAzules: 2, cartasVerdes: 2, cartasRosas: 0, cartasNegras: 0),
    ];
    var ronda3 = [
      Partida2(jugador: "Pedro", cartasAzules: 2, cartasVerdes: 2, cartasRosas: 2, cartasNegras: 2),
      Partida2(jugador: "Alfonso", cartasAzules: 3, cartasVerdes: 3, cartasRosas: 3, cartasNegras: 3),
    ];
    _infromacionPartida.add(
      charts.Series(
        domainFn: (Partida2 partida , _) => partida.jugador,
        measureFn: (Partida2 partida , _) => partida.cartasAzules,
        id: '1',
        data: ronda3,
        seriesCategory: '1',
        fillPatternFn: (_,__) => charts.FillPatternType.solid,
        fillColorFn: (Partida2 partida , _) => charts.ColorUtil.fromDartColor(Color(0xFF337CFF)),
      )
    );
    _infromacionPartida.add(
      charts.Series(
        domainFn: (Partida2 partida , _) => partida.jugador,
        measureFn: (Partida2 partida , _) => partida.cartasAzules,
        id: '1',
        data: ronda2,
        seriesCategory: '1',
        fillPatternFn: (_,__) => charts.FillPatternType.solid,
        fillColorFn: (Partida2 partida , _) => charts.ColorUtil.fromDartColor(Color(0xFF0F278A)),
      )
    );
    _infromacionPartida.add(
      charts.Series(
        domainFn: (Partida2 partida , _) => partida.jugador,
        measureFn: (Partida2 partida , _) => partida.cartasAzules,
        id: '1',
        data: ronda1,
        seriesCategory: '1',
        fillPatternFn: (_,__) => charts.FillPatternType.solid,
        fillColorFn: (Partida2 partida , _) => charts.ColorUtil.fromDartColor(Color(0xFF060F32)),
      )
    );

    _infromacionPartida.add(
      charts.Series(
        domainFn: (Partida2 partida , _) => partida.jugador,
        measureFn: (Partida2 partida , _) => partida.cartasVerdes,
        id: '1',
        data: ronda3,
        seriesCategory: '2',
        fillPatternFn: (_,__) => charts.FillPatternType.solid,
        fillColorFn: (Partida2 partida , _) => charts.ColorUtil.fromDartColor(Color(0xFF0BE700)),
      )
    );
    _infromacionPartida.add(
      charts.Series(
        domainFn: (Partida2 partida , _) => partida.jugador,
        measureFn: (Partida2 partida , _) => partida.cartasVerdes,
        id: '1',
        data: ronda2,
        seriesCategory: '2',
        fillPatternFn: (_,__) => charts.FillPatternType.solid,
        fillColorFn: (Partida2 partida , _) => charts.ColorUtil.fromDartColor(Color(0xFF044B00)),
      )
    );
    

    _infromacionPartida.add(
      charts.Series(
        domainFn: (Partida2 partida , _) => partida.jugador,
        measureFn: (Partida2 partida , _) => partida.cartasRosas,
        id: '1',
        data: ronda3,
        seriesCategory: '3',
        fillPatternFn: (_,__) => charts.FillPatternType.solid,
        fillColorFn: (Partida2 partida , _) => charts.ColorUtil.fromDartColor(Color(0xFFE42390)),
      )
    );
    

    _infromacionPartida.add(
      charts.Series(
        domainFn: (Partida2 partida , _) => partida.jugador,
        measureFn: (Partida2 partida , _) => partida.cartasNegras,
        id: '1',
        data: ronda3,
        seriesCategory: '4',
        fillPatternFn: (_,__) => charts.FillPatternType.solid,
        fillColorFn: (Partida2 partida , _) => charts.ColorUtil.fromDartColor(Color(0xFF4C4C4C)),
      )
    );
    
  }

  @override
  void initState(){
    super.initState();
    _llenarLista();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.cyan,
        title: Text("Informacion de la partida"),
      ),
      body:Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: Column(
                      children:[
                        Container(
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text('Se creo: 22/11/20'),
                            ),
                          ),
                        ),
                        posiciones(),
                        Container(
                          
                          child: Text(
                              'Cartas',
                              style: TextStyle(
                                fontSize: 24.0,
                                fontWeight: 
                                FontWeight.bold),
                                ),
                        ),
                        Container(
                          child: Expanded(
                            child: charts.BarChart(
                              _infromacionPartida,
                              animate: true,
                              barGroupingType: charts.BarGroupingType.groupedStacked,
                              animationDuration: Duration(seconds: 1),
                            ),
                          ),
                        ),
                        descripcion(),
                      ],
                    ),
                  ),
                ),
              ),
    ); 
  }
Widget posiciones(){
  return Container(
    child: Row(
    children: [
        Expanded(
          flex: 1,
          child: Container(
            child:primerlugar('Alfonso', '62 pts', 'Primero')
        ),
        ),
        SizedBox(
          height: 1,
          width: 1,
        ),
        Expanded(
          flex: 1,
          child: Container(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Container(
                      decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5.0))
              ),
              child: Column(
                children: [
                  perdedores('Pedro', '41 pts', 'segundo'),
                ],
              )
                    ),
                  ),
                ),
              ],
            ),
        ) 
        ),
      ],
    ),
  );
}
}

Widget primerlugar(String nombre, puntos, posicion){
  return Container(
              decoration: BoxDecoration(
                color: Colors.pink,
                borderRadius: BorderRadius.all(Radius.circular(5.0))
              ),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(child: Icon(TablerIcons.crown),),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Text(nombre,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                          ),),
                        )
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(puntos,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                        )
                        ),
                      ),
                    ],
                  ),
                ],
              )
            );
}

Widget perdedores(String nombre, puntos, posicion){
  return                 Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Center(
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.all(Radius.circular(5.0))
                      ),
                      child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(2.5),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(nombre,
                      style: TextStyle(
                        color: Colors.white
                      ),),
                    )
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.5),
                    child: Text(puntos,
                    style: TextStyle(
                        color: Colors.white
                    )
                    ),
                  ),
                ],
              )
                    ),
                  ),
                );
}


Widget descripcion(){
  return Container(
    child: Column(
      children: [
        Table(

          children: [
            TableRow(
              children:[
              Text('Azules:'),
              Row(
                children: [
              Container(
                height: 20,
                width: 20,
                color: Color(0xFF060F32),
              ),
              Text('Ronda 1'),
              ],
              ),
              Row(
                children: [
              Container(
                height: 20,
                width: 20,
                color: Color(0xFF0F278A),
              ),
              Text('Ronda 2'),
              ],
              ),
              Row(
                children: [
              Container(
                height: 20,
                width: 20,
                color: Color(0xFF337CFF),
              ),
              Text('Ronda 3'),
              ],
              ),
              ]
            ),
          ],
        ),
        Table(
          children: [
            TableRow(
              children:[
              Text('Verdes:'),
              Row(
                children: [
              Container(
                height: 20,
                width: 20,
                color: Color(0xFF044B00),
              ),
              Text('Ronda 2'),
              ],
              ),
              Row(
                children: [
              Container(
                height: 20,
                width: 20,
                color: Color(0xFF0BE700),
              ),
              Text('Ronda 3'),
              ],
              ),
              Row(
                children: [
              Container(
                height: 20,
                width: 20,
              ),
              ],
              ),
              ]
            ),
          ],
        ),
        Table(
          children: [
            TableRow(
              children:[
              Text('Rosas:'),
              Row(
                children: [
              Container(
                height: 20,
                width: 20,
                color: Color(0xFFE42390),
              ),
              Text('Ronda 3'),
              ],
              ),
              Row(
                children: [
              Container(
                height: 20,
                width: 20,
              ),
              ],
              ),
              Row(
                children: [
              Container(
                height: 20,
                width: 20,
              ),
              ],
              ),
              ]
            ),
          ],
        ),
        Table(
          children: [
            TableRow(
              children:[
              Text('Negras:'),
              Row(
                children: [
              Container(
                height: 20,
                width: 20,
                color: Color(0xFF4C4C4C),
              ),
              Text('Ronda 3'),
              ],
              ),
              Row(
                children: [
              Container(
                height: 20,
                width: 20,
              ),
              ],
              ),
              Row(
                children: [
              Container(
                height: 20,
                width: 20,
              ),
              ],
              ),
              ]
            ),
          ],
        ),
      ],
    ),
  );
}
class Partida2 {
  String jugador;
  int cartasAzules;
  int cartasVerdes;
  int cartasRosas;
  int cartasNegras;
  Partida2({
    required this.jugador,
    required this.cartasAzules,
    required this.cartasVerdes,
    required this.cartasRosas,
    required this.cartasNegras,
  });

}